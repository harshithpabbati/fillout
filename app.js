const express = require('express');
const axios = require('axios');
const dotenv = require('dotenv');

dotenv.config();
const app = express();
const port = process.env.PORT || 3000;

const fetchFormSubmissions = async (
  formId,
  {
    includeEditLink = false,
    sort = 'asc',
    afterDate,
    beforeDate,
    status = 'finished',
  }
) => {
  const url = new URL(
    `https://api.fillout.com/v1/api/forms/${formId}/submissions`
  );
  const params = { sort, status };
  if (includeEditLink) {
    // This is to avoid the editLink in the response, as the API returns the editLink even when we specify "includeEditLink=false"
    params.includeEditLink = true;
  }
  if (afterDate) params.afterDate = afterDate;
  if (beforeDate) params.beforeDate = beforeDate;
  url.search = new URLSearchParams(params).toString();

  try {
    const response = await axios.get(url, {
      headers: {
        Authorization: `Bearer ${process.env.FILLOUT_API_KEY}`,
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

const fetchAllSubmissions = async (formId, params) => {
  const responses = [];
  const limit = 150;
  let offset = 0;

  try {
    const {
      responses: initialResponses,
      totalResponses,
      pageCount,
    } = await fetchFormSubmissions(formId, { ...params, offset });
    responses.push(...initialResponses);

    if (pageCount > 1) {
      const pageCountWithoutInitial = pageCount - 1;
      const totalPagesToFetch = Math.min(
        pageCountWithoutInitial,
        Math.ceil(5 / limit)
      );
      const batchCount = Math.ceil(pageCountWithoutInitial / totalPagesToFetch);

      for (let batch = 1; batch <= batchCount; batch++) {
        const batchStartPage = (batch - 1) * totalPagesToFetch + 2;
        const batchEndPage = Math.min(
          batchStartPage + totalPagesToFetch - 1,
          pageCount
        );

        const batchPromises = [];

        for (let page = batchStartPage; page <= batchEndPage; page++) {
          offset = (page - 1) * limit;
          batchPromises.push(
            fetchFormSubmissions(formId, { ...params, offset })
          );
        }

        const batchResponses = await Promise.all(batchPromises);

        for (const batchResponse of batchResponses) {
          responses.push(...batchResponse.responses);
        }
        if (batch < batchCount) {
          // To respect the rate-limit of the Fillout APIs (5 requests per second)
          await new Promise((resolve) => setTimeout(resolve, 1000));
        }
      }
    }

    return { responses, totalResponses, pageCount };
  } catch (error) {
    throw error;
  }
};

const filterFormSubmissions = (data, { limit = 150, offset = 0, filters }) => {
  if (!filters)
    return {
      responses: data.responses.slice(offset, offset + limit),
      totalResponses: data.responses.length,
      pageCount: Math.ceil(data.responses.length / limit),
    };

  const filteredResponses = data.responses.filter((response) => {
    return JSON.parse(filters).every((filter) => {
      const field = response.questions.find(
        (question) => question.id === filter.id
      );
      const fieldValue = field?.value;

      if (fieldValue === undefined) return false;

      switch (filter.condition) {
        case 'equals':
          return fieldValue === filter.value;
        case 'does_not_equal':
          return fieldValue !== filter.value;
        case 'greater_than':
          if (field.type === 'DatePicker')
            return new Date(fieldValue) > new Date(filter.value);
          return parseFloat(fieldValue) > parseFloat(filter.value);
        case 'less_than':
          if (field.type === 'DatePicker')
            return new Date(fieldValue) < new Date(filter.value);
          return parseFloat(fieldValue) < parseFloat(filter.value);
        default:
          return false;
      }
    });
  });
  const responses = filteredResponses.slice(offset, offset + limit);
  return {
    responses,
    totalResponses: filteredResponses.length,
    pageCount: Math.ceil(filteredResponses.length / limit),
  };
};

app.get('/:formId/filteredResponses', async (req, res) => {
  try {
    // we fetch all the submissions made to a particular form
    const data = await fetchAllSubmissions(req.params.formId, req.query);
    const submissions = filterFormSubmissions(data, req.query);
    res.json(submissions);
  } catch (error) {
    console.error('Error fetching data from Fillout API:', error);
    res.status(500).json({ error });
  }
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
